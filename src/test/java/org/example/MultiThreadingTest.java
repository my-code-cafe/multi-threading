package org.example;

import jdk.incubator.concurrent.StructuredTaskScope;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.concurrent.*;
import java.util.function.Supplier;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class MultiThreadingTest {

    @Test
    void multiThreadUsingCompletableFutureAndForkJoinPool() {
        try (var forkJoinPool = new ForkJoinPool(3)) {
            CompletableFuture<String>[] futures = IntStream.rangeClosed(1, 1_000)
                    .boxed()
                    .map(this::task)
                    .map(task -> CompletableFuture.supplyAsync(task, forkJoinPool))
                    .<CompletableFuture<String>>toArray(CompletableFuture[]::new);

            CompletableFuture<Void> allOf = CompletableFuture.allOf(futures);

            allOf.join();

            Stream.of(futures).map(stringCompletableFuture -> {
                        try {
                            return stringCompletableFuture.get();
                        } catch (InterruptedException | ExecutionException e) {
                            throw new RuntimeException(e);
                        }
                    })
                    .distinct()
                    .toList().forEach(System.out::println);
        }
    }

    @Test
    void multiThreadingUsingStructuredTaskScope() throws InterruptedException, ExecutionException {
        try (var scope = new StructuredTaskScope<String>()) {
            List<Future<String>> tasks = IntStream.rangeClosed(1, 10_000)
                    .boxed()
                    .map(this::callableTask)
                    .map(scope::fork)
                    .toList();

            System.out.println("Started");
            scope.join();
            System.out.println("Completed - " + tasks.size());
            for (Future<String> task : tasks) {
                System.out.println(task.resultNow());
            }
        }
    }

    Callable<String> callableTask(int i) {
        return () -> task(i).get();
    }

    Supplier<String> task(int i) {
        return () -> {
            try {
                Thread.sleep(5);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            Thread currentThread = Thread.currentThread();
            return currentThread.toString();
        };
    }
}
